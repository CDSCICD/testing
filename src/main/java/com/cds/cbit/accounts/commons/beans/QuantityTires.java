package com.cds.cbit.accounts.commons.beans;

import com.cds.cbit.accounts.api.account.balance.flists.BalImpact;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The POJO represents field mapping of BRM QuantityTires.
 * 
 * @author  Meghashree udupa.
 * @version 1.0.
*/
@Data
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class QuantityTires implements Serializable {

  private static final long serialVersionUID = 1L;

  @XmlAttribute(name = "elem")
  private String elem;
  
  @XmlElement(name = "BAL_IMPACTS")
  private List<BalImpact> balImpacts;
} // End of QuantityTires.