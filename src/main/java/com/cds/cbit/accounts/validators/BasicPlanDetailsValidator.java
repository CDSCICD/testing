/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.validators;

import com.cds.cbit.accounts.api.customer.detail.flist.ExtraResults;
import com.cds.cbit.accounts.api.customer.detail.flist.LinkedObj;
import com.cds.cbit.accounts.api.customer.detail.flist.ResultsDynamo;
import com.cds.cbit.accounts.commons.beans.Args;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.BillingInfoSearchUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Component;

/**
 * The implementation component provides methods for validating account for BASE PLAN purchase 
 * in the system.
 * The component overrides BillingValidationInterface validateInput method to create and execute 
 * base plan search FList.If the validation is success then the method will return output FList else
 * it will throw an exception to the caller.
 * 
 * @author Meghashree Udupa
 *
 */

@Component("basicPlanValidator")
public class BasicPlanDetailsValidator implements BillingValidation {

  private final BillingInfoSearchUtil searchUtil;

  public BasicPlanDetailsValidator(BillingInfoSearchUtil searchUtil) {
    this.searchUtil = searchUtil;
  }

  @Override
  public FList validateInput(String input, String... data) throws EBufException, JAXBException {

    Args planObj = Args.builder().elem("1").planObj("").build();
    Args poidArgs = Args.builder().elem("2").poid("").build();
    Args accountObj = Args.builder().elem("3").accountObj(input).build();
    Args acctNoArgs = Args.builder().elem("4").status("3").build();
    Args createdArgs = Args.builder().elem("5").createdT(0L).build();
    Args serviceArg = Args.builder().elem("6").serviceObj("0.0.0.1 /service/telco/gsm/telephony -1")
        .build();
    
    List<Args> argsList = new ArrayList<>();
    argsList.add(planObj);
    argsList.add(poidArgs);
    argsList.add(accountObj);
    argsList.add(acctNoArgs);
    argsList.add(createdArgs);
    argsList.add(serviceArg);
    
    String basicPlantemplate = "";

    ResultsDynamo results = ResultsDynamo.builder().elem("0").quantity("0")
        .packageId(String.valueOf(0))
                               .purchaseEndT(0L).purchaseStartT(0L).planObj("").createdT(0L)
                    .linkedObj(LinkedObj.builder().elem("2").linkDirection("-1").planObj("")
                            .extraResults(ExtraResults.builder().elem("0").build()).build())
                    .cycleDiscount(new BigDecimal(0)).cycleDisAmt(new BigDecimal(0))
                    .cycleEndDetails(0)
                    .cycleFeeAmt(new BigDecimal(0)).cycleFeeFlags(0).cycleStartDetails(0)
                    .priceListName("").purchaseDiscount(new BigDecimal(0))
                    .purchaseDiscAmt(new BigDecimal(0))
                    .purchaseEndDetails(0).purchaseFeeAmt(new BigDecimal(0))
                    .purchaseStartDetails(0)
                    .usageDiscount(new BigDecimal(0)).usageEndDetails(0).usageStartDetails(0)
                    .endT("").build();
    
    if (data[0].equals("withOverride")) {
      basicPlantemplate = "select X from /purchased_product 1 , "
          + "/plan 2 where 1.F1=2.F2 and 1.F3=V3 and 1.F4 != V4 "
          + "order by F5 desc";
      results.setElem("*");
    } else {      
      argsList.add(serviceArg);
      basicPlantemplate = "select X from /purchased_product 1 , /plan 2 where 1.F1=2.F2 "
            + "and 1.F3=V3 and 1.F4 != V4 and 1.F6.type=V6 order by F5 desc";
    }
    
    Args[] arguments =  argsList.toArray(new Args[0]);
    
    return searchUtil.executeSearchTemplateDynamo(basicPlantemplate, arguments, results, "1280");

  } // End of validateInput method.
} // End of BasicPlanDetailsValidator class.