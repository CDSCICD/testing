/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.account.prepaid;

import com.cds.cbit.accounts.api.account.payload.AccountRequestBody;
import com.cds.cbit.accounts.api.account.payload.PlanInfo;
import com.cds.cbit.accounts.exceptions.BillingException;
import com.cds.cbit.accounts.properties.PrepaidProperties;
import com.cds.cbit.accounts.util.BrmServiceUtil;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalOp;
import com.portal.pcm.fields.FldAccountObj;
import com.portal.pcm.fields.FldBalInfo;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

/**
 * The component will provide methods to update the prepaid account created with the base plan 
 * information.
 * 
 * @author  Venkata Nagaraju.
 * @version 1.0.
*/
@Log4j2
@Component
public class PrepaidWorkFlowProcessor {
  
  @Autowired
  private BrmServiceUtil brmUtil;
  
  @Autowired
  private JdbcTemplate template;
  
  @Autowired
  private PrepaidProperties props;
  
  private final AccountUpdateProcessor processor;
  private final ComponentSubscriber subscriber;
  
  /** Constructor injection. **/
  public PrepaidWorkFlowProcessor(
         final AccountUpdateProcessor processor,final ComponentSubscriber subscriber) {
    
    this.processor = processor;
    this.subscriber = subscriber;
  } // End of constructor injection.
  
  /**
   * The method will update the prepaid account with the base plan information.
   * 
   * @param: accountInputs - @Mandatory - Prepaid account inputs.
  */
  public void processWorkFlow(final PrepaidAccountInput accountInputs,final String subType) 
                                             throws EBufException, JAXBException, ParseException {
    
    final AccountRequestBody reqBody = accountInputs.getReqBody();
    final String planType = accountInputs.getPlanType();
    
    final FList custCommit = accountInputs.getCustCommit();
    final Poid devicePoid = accountInputs.getDevicePoid();
    final FList planFlist = accountInputs.getPlanFlist();
    final PortalContext portal = accountInputs.getPortal();
    final String endT = accountInputs.getEndT();
    
    validatePlanAndUsageAmount(planType, reqBody);
    
    final Poid accountPoid  = 
          custCommit.get(FldBalInfo.getInst()).getValues().get(0).get(FldAccountObj.getInst());
    
    if (reqBody.getPlanInfo().getPrice() > 0) {
      postPaymentCollect(reqBody.getPlanInfo().getPrice(),accountPoid,endT,portal);
    }
    
    processor.updateAccountWithBasePlan(
                                    reqBody, custCommit, devicePoid, planFlist,endT,
                                    subType,portal);
    subscriber.subscribeComponents(
                               reqBody.getPlanInfo(),accountPoid, planType, planFlist,endT,
                               subType,portal);
  } // End of updateAccountWithBasePlan method.
  
  /**
   * The method will post payment in BRM against the base plan amount provided in the request as 
   * part of prepaid account creation.
   * 
   * @param: price        - @Mandatory - Base plan price given in the request.
   * @param: accountPoid  - @Mandatory - Account POID.
   * @param: endT         - @Mandatory - End date provided in the request.
  */
  private void postPaymentCollect(
          final double price,final Poid accountPoid,final String endT,final PortalContext portal)
                                            throws ParseException, JAXBException, EBufException {
    final String amount = String.valueOf(price);
    final BillPayment payment = 
          BillPayment.builder().amount(amount).command("0")
                     .currency(props.getCurrency()).payType(props.getPayCollectType())
                     .transId(String.valueOf(accountPoid.getId())).build();
    
    final BillReceivePayment paymentCollect =
          BillReceivePayment.builder().amount(amount).currency(props.getCurrency())
                            .name("Payment").payment(payment).poid(accountPoid.toString())
                            .programName("Payment").build();
    
    paymentCollect.setValidFrom(new Date(0).getTime() / 1000);
    paymentCollect.setValidTo(0L);
    if (endT.length() > 0) {
      paymentCollect.setEndT(UtcDateUtil.convertStringToTimeStamp(endT));
      paymentCollect.setValidFrom(UtcDateUtil.convertStringToTimeStamp(endT));
    } 
    final JAXBContext jaxbContext = JAXBContext.newInstance(BillReceivePayment.class);
    final FList paymentOp =  brmUtil.getFListFromPojo(jaxbContext,paymentCollect);
    log.info("BILL_RCV_PAYMENT{}",paymentOp);
    portal.opcode(PortalOp.BILL_RCV_PAYMENT, paymentOp);
  } // End of postPaymentCollect method.
  
  /** The method will verify whether first usage amount greater than plan amount or not. **/
  private void validatePlanAndUsageAmount(String planType,AccountRequestBody arguments) {
    try {
      if ("firstUsage".equals(planType)) {
        BigDecimal planAmount = BigDecimal.valueOf(arguments.getPlanInfo().getPrice());
        if (planAmount.compareTo(BigDecimal.ZERO) > 0) {
          String query = getFirstUsageAmount(arguments.getPlanInfo()
                                                     .getName(),arguments.getPlanInfo());
          log.info(query);
          template.query(query, new RowCallbackHandler() {
            public void processRow(ResultSet resultSet) throws SQLException {
              BigDecimal totalAmount = resultSet.getBigDecimal("amount");
              log.info("Firstusage amount : {}",totalAmount);
              log.info("Given amount : {}",planAmount);
              if (planAmount.compareTo(totalAmount) < 0) {
                throw new BillingException("200111");
              }
            }
          });
        } // End of plan amount existence check.
      } // End of first usage check.
    } catch (BillingException e) {
      throw new BillingException(e.getMessage());
    } catch (Exception e) {
      throw new BillingException("101102");
    }
  } // End of validatePlanAndUsageAmount.
  
  /**
   * Method to prepare query for fetching account first usage activation status.
   * @param- planName- Customer base plan.
  */
  public String getFirstUsageAmount(String planName,PlanInfo planInfo) {

    StringBuilder query = new StringBuilder(600);
    String planQuery = "select sum(rbi.scaled_amount) as AMOUNT from DEAL_PRODUCTS_T dp, "
            + " rate_plan_t rp, rate_t r, RATE_BAL_IMPACTS_T rbi where "
            + " dp.product_obj_id0 = rp.product_obj_id0 and rp.EVENT_TYPE "
            + " like '%cycle_forward%' and rp.poid_id0 = r.RATE_PLAN_OBJ_ID0 "
            + " and r.poid_id0 = rbi.obj_id0 and rbi.element_id = 36 and dp.obj_id0 in "
            + " ((select ps.deal_obj_id0 from plan_t pl, PLAN_SERVICES_DEALS_T ps "
            + " where pl.poid_id0 = ps.obj_id0 and pl.name = '" + planName + "')";
    query.append(planQuery);
    if (planInfo.getComponents() != null) {
      List<String> deals = new ArrayList<>();
      for (int j = 0;j < planInfo.getComponents().size();j++) {
        deals.add(planInfo.getComponents().get(j).getDealCode());
      }

      if (!deals.isEmpty()) {
        query.append(" union (select poid_id0 from deal_t where code in (");
        for (int i = 0; i < deals.size();i++) {
          query.append("'" + deals.get(i) + "'");
          if (i < deals.size() - 1) {
            query.append(',');
          }
        } // End of looping through deals.
        query.append("))");
      } // end If for component deals check.
    }
    query.append(")");
    return query.toString();
  } // End of getActivationStatus method.
} // End of AccountUpdateProcessor, which updates account with base plan for prepaid account.