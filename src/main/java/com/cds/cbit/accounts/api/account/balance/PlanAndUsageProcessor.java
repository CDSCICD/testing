/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.account.balance;

import com.cds.cbit.accounts.api.account.balance.flists.SubBalImpacts;
import com.cds.cbit.accounts.api.account.balance.payload.CustomerPlanInfo;
import com.cds.cbit.accounts.api.account.payload.OverrideDetail;
import com.cds.cbit.accounts.commons.beans.Args;
import com.cds.cbit.accounts.commons.beans.CycleFees;
import com.cds.cbit.accounts.commons.beans.Results;
import com.cds.cbit.accounts.factory.BillingValidationFactory;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.BillingInfoSearchUtil;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.fields.FldCycleFeeEndT;
import com.portal.pcm.fields.FldCycleFees;
import com.portal.pcm.fields.FldExtraResults;
import com.portal.pcm.fields.FldName;
import com.portal.pcm.fields.FldPlanObj;
import com.portal.pcm.fields.FldPoid;
import com.portal.pcm.fields.FldPriceListName;
import com.portal.pcm.fields.FldPurchaseEndT;
import com.portal.pcm.fields.FldPurchaseStartT;
import com.portal.pcm.fields.FldResults;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.bind.JAXBException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * The component provides methods to fetch plan details and usage details as part of the API call
 * GetCustomerBalances.
 * 
 * @author Venkata Nagaraju.
 * @version 1.0.
 */

@Component
public class PlanAndUsageProcessor {

  @Autowired
  private Environment props;
  
  @Autowired
  private BillingValidationFactory factory;

  private final CustomerDetailsRetriever customerDetail;
  private final BillingInfoSearchUtil searchUtil;

  /**
   * constructor injection.
   */
  public PlanAndUsageProcessor(final CustomerDetailsRetriever customerDetail,
      final BillingInfoSearchUtil searchUtil) {

    this.customerDetail = customerDetail;
    this.searchUtil = searchUtil;
  } // End of constructor injection.

  /**
   * The method will fetch from,end date information along with customer used and unused amounts.
   * 
   * @param: subBalance
   *           - @Mandatory - SubBalanceImpacts POJO.
   * @param: balIndex
   *           - @Mandatory - Index of current balance element of the loop.
   */
  protected Map<String, Object> getDateAndUsage(final SubBalImpacts subBalance,
      final String balIndex) {

    // Fetching external id with the current balance index, from the config file.
    final Map<String, Object> dateAndUsage = new ConcurrentHashMap<>();
    final StringBuilder externalId = new StringBuilder("msg.externalId");
    final Optional<String> externalIdVal = Optional
        .ofNullable(props.getProperty(externalId.append(balIndex).toString()));

    // Fetching current and available balances from the sub balances section.
    final BigDecimal availableBalance = new BigDecimal(subBalance.getAmount()).setScale(2,
        BigDecimal.ROUND_HALF_UP);
    final BigDecimal currentBalance = new BigDecimal(subBalance.getAmount());

    if (externalIdVal.isPresent()) {
      dateAndUsage.put("externalId", externalIdVal.get());
    }
    dateAndUsage.put("availableBalance", availableBalance);
    dateAndUsage.put("currentBalance", currentBalance);

    // Fetching fromDate and endDate of the sub balances.
    final Long validFrom = subBalance.getValidFrom();
    final String validFromDate = UtcDateUtil.convertDateToUtc(new Date(validFrom * 1000));

    final Optional<Long> toDate = Optional.ofNullable(subBalance.getValidTo());
    if (toDate.isPresent()) {
      Long validTo = toDate.get() * 1000;
      if (new DateTime(validTo).getYear() != 1970) {
        validTo = validTo - 1000;
      }

      final String validToDate = UtcDateUtil.convertDateToUtc(new Date(validTo));
      dateAndUsage.put("validTo", validToDate);
    }
    dateAndUsage.put("validFrom", validFromDate);
    return dateAndUsage; // Returning customer current, available balance amount with dates.
  } // End of getDateAndUsage method.

  /**
   * The method will fetch plan information of "GetCustomerBalance" API for both with and without
   * MSISDN association for the given account.
   * 
   * @param: accountObj
   *           - @Mandatory - Account POID of requested account number.
   */
  protected List<CustomerPlanInfo> fetchPlanDetails(final String accountObj, final String subType)
      throws EBufException, JAXBException {
    final FList packageOp = customerDetail.fetchPackagedata(accountObj);

    List<CustomerPlanInfo> planInfoList = new ArrayList<>();
    CustomerPlanInfo planInfo = new CustomerPlanInfo();

    if (packageOp.hasField(FldResults.getInst())) { // Proceed, if package FList as result field.
      // Looping through customer packages results.
      for (int i = 0; i < packageOp.get(FldResults.getInst()).getValues().size(); i++) {
        List<FList> packageDetail = packageOp.get(FldResults.getInst()).getValues();

        for (FList extraResults : packageOp.get(FldExtraResults.getInst()).getValues()) {
          if (extraResults.get(FldPoid.getInst())
              .equals(packageDetail.get(0).get(FldPlanObj.getInst()))) {
            planInfo.setName(extraResults.get(FldName.getInst()));
            planInfo.setFromDate(UtcDateUtil
                .convertDateToUtc(packageDetail.get(0).get(FldPurchaseStartT.getInst())));
            if (packageDetail.get(0).containsKey(FldPurchaseEndT.getInst())) {
              long validTo = packageDetail.get(0).get(FldPurchaseEndT.getInst()).getTime();
              if (new DateTime(validTo).getYear() != 1970) {
                validTo = validTo - 1000;
              }
              planInfo.setToDate(UtcDateUtil.convertDateToUtc(new Date(validTo)));
            } // End of end date check.

            planInfo.setOverrideDetail(setOverrideDetails(accountObj,
                                               packageDetail.get(0).get(FldPlanObj.getInst())));
            break;
          }
        }
      } // End of looping through package details results.

      if ("PREPAID".equals(subType)) {
        planInfo.setToDate(UtcDateUtil.convertDateToUtc(fetchCycleEndT(accountObj)));
      }
      planInfoList.add(planInfo);
    } // End of result field check for plan details FList.

    return planInfoList;
  } // End of fetchPlanDetails method.

  private OverrideDetail setOverrideDetails(String accountPoid,Poid planPoid) 
                                                     throws EBufException, JAXBException {

    OverrideDetail overrideDetail = new OverrideDetail();
    
    final BillingValidation basicPlanValidator = factory.getValidator("basicPlanValidator");
    final FList baseplanOp = basicPlanValidator.validateInput(
                                                String.valueOf(accountPoid),"withOverride");
    if (baseplanOp.hasField(FldResults.getInst())) {
  
      for (FList basePlanResult : baseplanOp.get(FldResults.getInst()).getValues()) {
        if (basePlanResult.get(FldPlanObj.getInst()).equals(planPoid) 
                      && (basePlanResult.get(FldPriceListName.getInst()) != null 
                      && !basePlanResult.get(FldPriceListName.getInst()).isEmpty())) {
          overrideDetail.setRefId(basePlanResult.get(FldPriceListName.getInst()));
          break;
        }
      }
    }
    return overrideDetail;
  }

  /**
   * Method which will fetch cycle fees endT from BRM.
   * 
   * @param- accountPoid
   * @return- cycleFeeEndT
   * @throws- EBufException,JAXBException
   */
  private Date fetchCycleEndT(String accountPoid) throws EBufException, JAXBException {

    final Args accountArgs = Args.builder().elem("1").accountObj(accountPoid).build();

    Date cycleEndT = new Date(0);
    final Args[] arguments = { accountArgs };

    final String tempelate = "select X from /purchased_product where F1 = V1 and "
        + "plan_obj_id0 !=0 and service_obj_type = '/service/telco/gsm/telephony' "
        + " and status != 3 order by created_t asc";

    CycleFees cycleFees = CycleFees.builder().elem("1").feeEndT("").feeStartT("").build();
    final Results results = Results.builder().elem("0").accountObj("").createdT(0L).poid("")
        .planObj("").packageId(String.valueOf(0)).cycleEndT("").cycleStartT("")
        .cycleFees(Arrays.asList(cycleFees)).build();

    final FList output = searchUtil.executeSearchTemplate(tempelate, arguments, results);

    if (output.hasField(FldResults.getInst()) 
        && output.get(FldResults.getInst()).getValues().get(0).hasField(FldCycleFees.getInst())) {
      cycleEndT = output.get(FldResults.getInst()).getValues().get(0).get(FldCycleFees.getInst())
                                               .getValues().get(0).get(FldCycleFeeEndT.getInst());
    }

    long validTo = cycleEndT.getTime();
    if (new DateTime(validTo).getYear() != 1970) {
      validTo = validTo - 1000;
    }
    return new Date(validTo);
  } // End of fetchCycleEndT.
} // End of BalanceDatesAndAmount, which retrieve dates and amounts required for customer balances.