/*
 * Copyright (C) 2019 Covalensedigital 

 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.customer.detail.payload;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The POJO provide field mapping to PortinInfo section of account creation
 * response JSON.
 * 
 * @author Saibabu Guntur
 * @Version 1.0.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PortinInfo implements Serializable {

  private static final long serialVersionUID = 3474566348136394846L;

  private String date; // Portin date.

  private String donor; // Portin donor.

  private String msisdn; // Portin phone number.
} // End of PortinInfo.