/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.validators;

import com.cds.cbit.accounts.commons.beans.Args;
import com.cds.cbit.accounts.commons.beans.Results;
import com.cds.cbit.accounts.commons.beans.UsageMap;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.BillingInfoSearchUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Component;

/**
 * The implementation component provides methods for validating given base plan in the system.
 * The component overrides BillingValidationInterface validateInput method to create and execute 
 * base plan search FList.If the validation is success then the method will return output FList 
 * else it will throw an exception to the caller.
 * 
 * @author  Meghashree Udupa.
 * @version 1.0.
*/
@Component("fetchProductType")
public class FetchProductType implements BillingValidation {
  
  private final BillingInfoSearchUtil searchUtil;  // Util class to execute search opcode.

  public FetchProductType(final BillingInfoSearchUtil searchUtil) {
    
    this.searchUtil = searchUtil;
  } // End of Constructor injection.
  
  @Override
  public FList validateInput(final String input,final String... data) 
                                                               throws EBufException, JAXBException {
    final Args[] args = { Args.builder().elem("1").poid(input).build() };

    final UsageMap usageMap = UsageMap.builder().elem("*").eventType("").build();
    final Results results = Results.builder().elem("0").usageMap(usageMap).build();
    String template = "select X from /product where F1 = V1";
    
    return searchUtil.executeSearchTemplate(template,args,results,"256");
  } // End of validateInput.
} // End of FetchProductType