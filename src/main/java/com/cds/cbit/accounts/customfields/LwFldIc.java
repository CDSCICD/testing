package com.cds.cbit.accounts.customfields;

import com.portal.pcm.StrField;

/**
 * Specific Field subclasses. This subclasses of <code>Field</code> is used with the FList class to
 * specifiy which field is being accessed, and its type. The type information is used to provide
 * compile time type checking. These classes are auto generated.
 * 
 * @version 1.0 Thu Mar 22 17:09:35 IST 2018
 * @author Autogenerated
 */

public class LwFldIc extends StrField {

  private static final long serialVersionUID = 1L;

  /**
   * Constructs an instance of <code>LwFldIc</code>.
   */
  public LwFldIc() {
    super(20005, 5);
  }

  /**
   * Returns an instance of <code>LwFldIc</code>.
   * 
   * @return An instance of <code>LwFldIc</code>.
   */
  public static synchronized LwFldIc getInst() {
    if (me == null) {
      me = new LwFldIc();
    }
    return me;
  }

  private static LwFldIc me;
}
