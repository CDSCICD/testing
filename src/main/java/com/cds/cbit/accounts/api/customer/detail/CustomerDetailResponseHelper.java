package com.cds.cbit.accounts.api.customer.detail;

import com.cds.cbit.accounts.api.customer.detail.payload.Address;
import com.cds.cbit.accounts.api.customer.detail.payload.Ids;
import com.cds.cbit.accounts.api.customer.detail.payload.Name;
import com.cds.cbit.accounts.api.customer.detail.payload.OrderInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.PaymentInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.PersonalInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.ProfileData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.fields.FldActgCycleDom;
import com.portal.pcm.fields.FldAddress;
import com.portal.pcm.fields.FldCity;
import com.portal.pcm.fields.FldCompany;
import com.portal.pcm.fields.FldContentCategoryName;
import com.portal.pcm.fields.FldCountry;
import com.portal.pcm.fields.FldEmailAddr;
import com.portal.pcm.fields.FldExtraResults;
import com.portal.pcm.fields.FldFirstName;
import com.portal.pcm.fields.FldLastName;
import com.portal.pcm.fields.FldMiddleName;
import com.portal.pcm.fields.FldName;
import com.portal.pcm.fields.FldNameinfo;
import com.portal.pcm.fields.FldNoteStr;
import com.portal.pcm.fields.FldProfileDataArray;
import com.portal.pcm.fields.FldResults;
import com.portal.pcm.fields.FldSalutation;
import com.portal.pcm.fields.FldState;
import com.portal.pcm.fields.FldValue;
import com.portal.pcm.fields.FldZip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The component will help to prepare the response for customer detail API,
 * It will prepare all the details related to profile of the customer from billing system .
 * 
 * @author Meghashree Udupa.
 * @version 1.0
 */
@Component
public class CustomerDetailResponseHelper {

  public static final String NAME_INFO = "nameDetail";
  
  @Autowired
  ObjectMapper mapper;
  /**
   * The method getProfileDetail will prepare all the required response for customer detail
   * API from the billing system.
   * 
   * @param: responseDetails : map which contains all the details.
   * @param: nameFlist : FLIST which contains name information.
   * @param: address :  POJO which contains address information.
   * @param: name : POJO which contains name information.
   */
  
  public Map<String, Object> getProfileDetail(Map<String, Object> responseDetails) 
                                                                          throws EBufException {

    FList profiledetailsFlist = (FList) responseDetails.get("profileDetails");
     
    List<Ids> idsList = new ArrayList<>();
    List<FList> detailList = profiledetailsFlist.get(FldResults.getInst()).getValues().get(0)
                                                  .get(FldProfileDataArray.getInst()).getValues();

    Map<String,Object> profileMap = new HashMap<>();
    List<ProfileData> profileDataList = new ArrayList<>();
    
    for (int i = 0; i < detailList.size(); i++) {
      
      ProfileData profileData = new ProfileData();
      profileData.setName(detailList.get(i).get(FldName.getInst()));
      profileData.setValue(detailList.get(i).get(FldValue.getInst()));
    
      profileDataList.add(profileData);
      profileMap.put(profileData.getName(), profileData.getValue());
      
      if (detailList.get(i).get(FldContentCategoryName.getInst()).contains("ids")) {
        Ids ids = new Ids();
        ids.setType(detailList.get(i).get(FldNoteStr.getInst()));
        ids.setName(detailList.get(i).get(FldName.getInst()));
        ids.setValue(detailList.get(i).get(FldValue.getInst()));
        idsList.add(ids);
      }
      profileMap.put("ids", idsList);
      Address address = mapper.convertValue(profileMap,Address.class);
      profileMap.put("address", address);
      
      OrderInfo orderInfo = mapper.convertValue(profileMap,OrderInfo.class);
      profileMap.put("orderInfo", orderInfo);
       
      PaymentInfo paymentInfo = mapper.convertValue(profileMap,PaymentInfo.class);
      profileMap.put("paymentInfo", paymentInfo);
      
      profileMap.values().removeIf(Objects::isNull);
      
    }
    PersonalInfo personalInfo = mapper.convertValue(profileMap,PersonalInfo.class);
    Address address = getAddress(responseDetails,personalInfo);
    personalInfo.setAddress(address);
   
    Name name = getName(responseDetails);
    personalInfo.setName(name);
    
    FList namedetail = (FList) responseDetails.get(NAME_INFO);
    String displayName = "";
    String email = "";
    String billingDate = "";
    if (namedetail.hasField(FldResults.getInst())) {
      FList nameFlist = namedetail.get(FldResults.getInst()).getValues().get(0)
                                                    .get(FldNameinfo.getInst()).getValues().get(0);
      displayName  = nameFlist.get(FldCompany.getInst());
      email  = nameFlist.get(FldEmailAddr.getInst());
      billingDate = namedetail.get(FldExtraResults.getInst()).getValues().get(0)
                                                              .get(FldActgCycleDom.getInst()) + "";
    }
    personalInfo.setEmail(email);
    personalInfo.setDisplayName(displayName);
    personalInfo.setBillingDate(billingDate);
    
    Map<String, Object> infoMap = new HashMap<>();
    infoMap.put("personalInfo", personalInfo);
    
    infoMap.put("paymentFlag", true);
    infoMap.put("paymentInfo", personalInfo.getPaymentInfo());
   
    OrderInfo orderInfo = new OrderInfo();
    orderInfo.setNumber(personalInfo.getOrderInfo().getOrderNumber());
    orderInfo.setCheckoutDate(personalInfo.getOrderInfo().getCheckoutDate());
    orderInfo.setReferralCode(personalInfo.getOrderInfo().getReferralCode());
    
    personalInfo.setOrderInfo(orderInfo);
    infoMap.put("orderInfo", personalInfo.getOrderInfo());
    return infoMap;
  }

  /** Method to prepare address information from billing system.  */
  private Address getAddress(Map<String, Object> responseDetails,
                                         PersonalInfo personalInfo) throws EBufException {
    FList namedetail = (FList) responseDetails.get(NAME_INFO);
    if (namedetail.hasField(FldResults.getInst())) {
      FList nameFlist = namedetail.get(FldResults.getInst()).getValues().get(0)
          .get(FldNameinfo.getInst()).getValues().get(0);
      
      return Address.builder().city(nameFlist.get(FldCity.getInst()))
          .country(nameFlist.get(FldCountry.getInst())).postalCode(nameFlist.get(FldZip.getInst()))
              .state(nameFlist.get(FldState.getInst())).street(nameFlist.get(FldAddress.getInst()))
                                             .buildingNo(personalInfo.getAddress().getBuildingNo())
                                                       .floor(personalInfo.getAddress().getFloor())
                                                .unit(personalInfo.getAddress().getUnit()).build();
    } else {
      return Address.builder().build();
    }
    
  } // End of getAddress method.

  /** Method to prepare name information from billing system. */
  private Name getName(Map<String, Object> responseDetails) throws EBufException {
    FList namedetail = (FList) responseDetails.get(NAME_INFO);
    if (namedetail.hasField(FldResults.getInst())) {
      FList nameFlist = namedetail.get(FldResults.getInst()).getValues().get(0)
          .get(FldNameinfo.getInst()).getValues().get(0);
      return Name.builder().first(nameFlist.get(FldFirstName.getInst()))
          .last(nameFlist.get(FldLastName.getInst())).middle(nameFlist.get(FldMiddleName.getInst()))
                                        .salutation(nameFlist.get(FldSalutation.getInst())).build();
    } else {
      return Name.builder().build();
    }
   
  } // End of getName method.

  
} // End of CustomerDetailResponseHelper class.