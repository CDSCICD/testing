package com.cds.cbit.accounts.customfields;

import com.portal.pcm.StrField;

/**
 * Specific Field subclasses. This subclasses of <code>Field</code> is used with the FList class to
 * specifiy which field is being accessed, and its type. The type information is used to provide
 * compile time type checking. These classes are auto generated.
 * 
 * @version 1.0 Thu Mar 22 17:09:35 IST 2018
 * @author Autogenerated
 */

public class LwFldPortingNumber extends StrField {

  private static final long serialVersionUID = 1L;

  /**
   * Constructs an instance of <code>LwFldPortingNumber</code>.
   */
  public LwFldPortingNumber() {
    super(20016, 5);
  }

  /**
   * Returns an instance of <code>LwFldPortingNumber</code>.
   * 
   * @return An instance of <code>LwFldPortingNumber</code>.
   */
  public static synchronized LwFldPortingNumber getInst() {
    if (me == null) {
      me = new LwFldPortingNumber();
    }
    return me;
  }

  private static LwFldPortingNumber me;
}
