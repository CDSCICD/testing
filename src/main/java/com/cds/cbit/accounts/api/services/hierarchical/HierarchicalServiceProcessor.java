/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.services.hierarchical;

import com.cds.cbit.accounts.interfaces.AccountServices;
import com.cds.cbit.accounts.interfaces.GenericResponse;
import com.portal.pcm.EBufException;

import javax.xml.bind.JAXBException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * The component class will provide the business logic for fetching account details from the system.
 * The class first validate given account number and if it is valid then it will fetch the details
 * of the given account from billing system.
 * 
 * @author  Venkata Nagaraju.
 * @version 1.0
*/
@Component("hierarchicalAccountDetails")  // Same bean name will be used for flat account.
@ConditionalOnProperty(prefix = "account", name = "type", havingValue  = "hierarchical")
public class HierarchicalServiceProcessor implements AccountServices {
 
  /* @see com.cds.cbit.accounts.interfaces.AccountServices#processAccountService(java.lang.String)*/
  @Override
  public GenericResponse processAccountService(final String accountNo) 
                                                              throws EBufException, JAXBException {
    return null;
  } // End of processing account details request.
} // End of AccountDetailsService for fetching account details.