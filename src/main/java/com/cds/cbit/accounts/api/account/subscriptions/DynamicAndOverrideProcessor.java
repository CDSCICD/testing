/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.account.subscriptions;

import com.cds.cbit.accounts.api.account.flist.ProductsOrDiscounts;
import com.cds.cbit.accounts.api.account.payload.DiscountDetails;
import com.cds.cbit.accounts.api.account.payload.OverrideDetail;
import com.cds.cbit.accounts.exceptions.BillingException;
import com.cds.cbit.accounts.factory.BillingValidationFactory;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.cds.cbit.accounts.validators.ProductCurrencyValidator;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.fields.FldBalImpacts;
import com.portal.pcm.fields.FldCycleDiscAmt;
import com.portal.pcm.fields.FldCycleDiscount;
import com.portal.pcm.fields.FldCycleFeeAmt;
import com.portal.pcm.fields.FldDescr;
import com.portal.pcm.fields.FldEventType;
import com.portal.pcm.fields.FldPriceListName;
import com.portal.pcm.fields.FldProductObj;
import com.portal.pcm.fields.FldPurchaseDiscAmt;
import com.portal.pcm.fields.FldPurchaseDiscount;
import com.portal.pcm.fields.FldPurchaseFeeAmt;
import com.portal.pcm.fields.FldQuantityTiers;
import com.portal.pcm.fields.FldResults;
import com.portal.pcm.fields.FldStatusFlags;
import com.portal.pcm.fields.FldUsageDiscount;
import com.portal.pcm.fields.FldUsageMap;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;

import javax.xml.bind.JAXBException;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * The component is used to process dynamic discounting and override details for advance payment,
 * add on deal purchase and component deal purchase of account creation.
 * 
 * @author Meghashree udupa.
 * @version 1.0
 */

@Log4j2
@Component
public class DynamicAndOverrideProcessor {

  @Autowired
  private ProductCurrencyValidator productCurrencyValidator;
  
  @Autowired
  private BillingValidationFactory factory;

  @Autowired
  private Environment properties;
  
  private static final String AMOUNT = "amount";
  private static final String PERCENTAGE = "percentage";
  private static final String OVERRIDE = "override";
  private static final String TELEPHONY = "telephony";

  /** Method to process dynamic discounting. */
  public void addDynamicDiscount(ProductsOrDiscounts productOrDiscount,
      DiscountDetails discountDetails, FList dealElementFlist)
      throws ParseException, EBufException, JAXBException {
    if (discountDetails != null) {
      validateDynamicDiscount(discountDetails);
      
      if (discountDetails.getAmount() != null && !discountDetails.getAmount().isEmpty()) {

        boolean currencyProd = validateCurrencyProduct(dealElementFlist);

        if (currencyProd) {
          
          long endDate = 0L;
          if (discountDetails.getEndDate() != null) {
            endDate = UtcDateUtil.convertToUtcDate(discountDetails.getEndDate()).getTime() / 1000;
          }
          productOrDiscount.setDiscountEndT(String.valueOf(endDate));
          dealElementFlist.set(FldDescr.getInst(), String.valueOf(endDate));
        
          fetchEventType(productOrDiscount, dealElementFlist, discountDetails, 
              null, AMOUNT);
        }

      } else if (discountDetails.getPercentage() != null
          && !discountDetails.getPercentage().isEmpty()) {

        boolean currencyProd = validateCurrencyProduct(dealElementFlist);
        if (currencyProd) {
         
          long endDate = 0L;
          if (discountDetails.getEndDate() != null) {
            endDate = UtcDateUtil.convertToUtcDate(discountDetails.getEndDate()).getTime() / 1000;
          }
          productOrDiscount.setDiscountEndT(String.valueOf(endDate));
          dealElementFlist.set(FldDescr.getInst(), String.valueOf(endDate));
          
          fetchEventType(productOrDiscount, dealElementFlist, discountDetails, 
              null, PERCENTAGE);
        }
      }
    }
  }

  private void setOneTimeOrCycle(String eventType,ProductsOrDiscounts productOrDiscount,
      FList dealElementFlist,DiscountDetails discountDetails,BigDecimal price,String flag) {
    if (eventType.contains("/event/billing/product/fee/purchase")) {
   
      if (flag.equals(AMOUNT)) {
        productOrDiscount.setPurchaseDiscAmt(discountDetails.getAmount());
        dealElementFlist.set(FldPurchaseDiscAmt.getInst(),
            new BigDecimal(discountDetails.getAmount()));
      } else if (flag.equals(PERCENTAGE)) {
        productOrDiscount.setPurchaseDiscount(new BigDecimal(discountDetails
            .getPercentage()).divide(new BigDecimal(100)));
        dealElementFlist.set(FldPurchaseDiscount.getInst(),
            new BigDecimal(discountDetails
                .getPercentage()).divide(new BigDecimal(100)));
      } else if (flag.equals(OVERRIDE)) {
        productOrDiscount.setPurchaseFeeAmt(price);
        
        dealElementFlist.set(FldPurchaseFeeAmt.getInst(), price);
        
        productOrDiscount.setStatusFlags(16777216);
        dealElementFlist.set(FldStatusFlags.getInst(), 16777216);
      }
    }
    if (eventType.contains("/event/billing/product/fee/cycle")) {
      if (flag.equals(AMOUNT)) {
        productOrDiscount.setDiscountAmount(discountDetails.getAmount());
        dealElementFlist.set(FldCycleDiscAmt.getInst(),
            new BigDecimal(discountDetails.getAmount()));
      } else if (flag.equals(PERCENTAGE)) {
        productOrDiscount.setCycleDiscount(new BigDecimal(discountDetails
            .getPercentage()).divide(new BigDecimal(100)));
        dealElementFlist.set(FldCycleDiscount.getInst(),
            new BigDecimal(discountDetails
                .getPercentage()).divide(new BigDecimal(100)));
      } else if (flag.equals(OVERRIDE)) {
        productOrDiscount.setCycleFeeAmt(price);
        
        dealElementFlist.set(FldCycleFeeAmt.getInst(), price);
        
        productOrDiscount.setStatusFlags(33554432);
        dealElementFlist.set(FldStatusFlags.getInst(), 33554432);
      }
    }
    
  }

  /** Method to validate dynamic discounting . */
  private void validateDynamicDiscount(DiscountDetails discountDetails) {

    if (discountDetails.getAmount() == null && discountDetails.getPercentage() == null) {
      throw new BillingException("200110");
    } else if (discountDetails.getAmount() != null
        && new BigDecimal(discountDetails.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
      throw new BillingException("200108");
    } else if (discountDetails.getPercentage() != null
        && new BigDecimal(discountDetails.getPercentage()).compareTo(BigDecimal.ZERO) < 0) {
      throw new BillingException("200109");
    }
  }

  /** Method to validate currency Product. */
  public boolean validateCurrencyProduct(FList dealElementFlist)
      throws EBufException, JAXBException {

    boolean currencyProd = false;
    FList output = productCurrencyValidator
        .validateInput(dealElementFlist.get(FldProductObj.getInst()).toString(), 36);

    if (output.hasField(FldResults.getInst())) {
      currencyProd = true;
    }
    return currencyProd;
  }

  /** Method to fetchProductType of Product.  */
  public FList fetchProductType(FList dealElementFlist) throws EBufException, JAXBException {
    final BillingValidation validator = factory.getValidator("fetchProductType");
  
    return validator.validateInput(String
        .valueOf(dealElementFlist
            .get(FldProductObj.getInst())));
  }
  /**
   * Method to add override data to product sparse.
   * 
   * @param- productsOrDiscounts,overrideData
   * @throws- ParseException
   */
  
  public void addOverrideDetail(ProductsOrDiscounts productsOrDiscounts, FList dealElementFlist,
      OverrideDetail overrideData, String dealService,final String subType)
      throws ParseException, EBufException, JAXBException {
    boolean refId = false;
    if (overrideData != null) {
      boolean currencyProd = false;
      FList output = productCurrencyValidator
          .validateInput(dealElementFlist.get(FldProductObj.getInst()).toString(), 
              Integer.valueOf(properties.getProperty("currency")));
                                        
      if (output.hasField(FldResults.getInst())) {
        currencyProd = true;
      }
      
      if (currencyProd && overrideData.getPrice() != null) {
        refId = true;
        
        BigDecimal price = new BigDecimal(overrideData.getPrice());
        
        if (subType  != null && subType.equals("POSTPAID")) {
          price = price.divide(new BigDecimal(properties
              .getProperty("tax.deduction")),6, RoundingMode.CEILING);
        }
        price = price.setScale(6,RoundingMode.HALF_UP);
        
       
        
        fetchEventType(productsOrDiscounts, dealElementFlist, null, 
            price, OVERRIDE);
        
        log.info("Override data found");
      }
      
      if (output.hasField(FldResults.getInst())  && output.get(FldResults.getInst()).getValues()
                          .get(0).get(FldQuantityTiers.getInst())
                                 .getValues().get(0).get(FldBalImpacts.getInst()).size() > 1) {
        currencyProd = false;
      }

      if (!currencyProd && overrideData.getLocal() != null) {
        refId = checkLocal(productsOrDiscounts,dealElementFlist,overrideData,dealService);
      }
     
      if (!currencyProd && overrideData.getIdd() != null) {
        refId = checkIdd(productsOrDiscounts,dealElementFlist,overrideData,dealService);
      }

      if (!currencyProd && overrideData.getRoaming() != null) {
        refId = checkRoaming(productsOrDiscounts,dealElementFlist,overrideData,dealService);
      }
      if (refId) {
        productsOrDiscounts.setRefId(overrideData.getRefId());
        dealElementFlist.set(FldPriceListName.getInst(),overrideData.getRefId());
      }
    }
  }

  private void fetchEventType(ProductsOrDiscounts productsOrDiscounts,
      FList dealElementFlist, DiscountDetails discountDetails, BigDecimal price, String flag) 
          throws EBufException, JAXBException {
    
    FList productType = fetchProductType(dealElementFlist);
    FList results = productType.get(FldResults.getInst()).getValues().get(0);
    
    for (int i = 0; i < results.get(FldUsageMap.getInst()).getSize(); i++) {
      String eventType = results.get(FldUsageMap.getInst()).getValues().get(i)
          .get(FldEventType.getInst());
      setOneTimeOrCycle(eventType, productsOrDiscounts, dealElementFlist, discountDetails, 
          price, flag);
    }
  }

  private boolean checkRoaming(ProductsOrDiscounts productsOrDiscounts, FList dealElementFlist,
      OverrideDetail overrideData, String dealService) {
    boolean refId = false;
    log.info("Override data found for roaming");
    if (dealService.contains("gprs") && overrideData.getLocal()
        .getData() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getRoaming()
                                                                              .getData()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getRoaming().getData()).negate());
    } else if (dealService.contains("sms") && overrideData.getLocal().getSms() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getRoaming()
                                                                               .getSms()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getRoaming().getSms()).negate());
    } else if (dealService.contains("mms") && overrideData.getLocal()
        .getMms() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getRoaming()
                                                                               .getMms()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getRoaming().getMms()).negate());
    } else if (dealService.contains(TELEPHONY) && overrideData.getLocal()
        .getVoicePool() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getRoaming()
                                                                         .getVoicePool()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getRoaming().getVoicePool()).negate());
    }
    return refId;
     
  }

  private boolean checkLocal(ProductsOrDiscounts productsOrDiscounts, FList dealElementFlist,
      OverrideDetail overrideData, String dealService) {
    boolean refId = false;
    log.info("Override data found for local");
    if (dealService.contains("gprs") && overrideData.getLocal()
        .getData() != null) {
     
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getLocal()
                                                                             .getData()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getLocal().getData()).negate());

    } else if (dealService.contains("sms") && overrideData.getLocal()
                                                                              .getSms() != null) {
      
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getLocal()
                                                                              .getSms()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getLocal().getSms()).negate());
    } else if (dealService.contains("mms") && overrideData.getLocal()
        .getMms() != null) {
     
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getLocal()
                                                                              .getMms()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getLocal().getMms()).negate());
    } else if (dealService.contains(TELEPHONY) && overrideData.getLocal()
        .getVoicePool() != null) {
 
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getLocal()
                                                                        .getVoicePool()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getLocal().getVoicePool()).negate());
    }
    return refId;
    
  }

  private boolean checkIdd(ProductsOrDiscounts productsOrDiscounts, FList dealElementFlist,
                                                 OverrideDetail overrideData, String dealService) {
    boolean refId = false;
    log.info("Override data found for idd");
    if (dealService.contains("gprs") && overrideData.getLocal()
        .getData() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getIdd()
                                                                              .getData()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getIdd().getData()).negate());
    } else if (dealService.contains("sms") && overrideData.getLocal()
        .getSms() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getIdd().getSms()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getIdd().getSms()).negate());
    } else if (dealService.contains("mms") && overrideData.getLocal()
        .getMms() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getIdd().getMms()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getIdd().getMms()).negate());
    } else if (dealService.contains(TELEPHONY) && overrideData.getLocal()
        .getVoicePool() != null) {
      refId = true;
      productsOrDiscounts.setUsageDiscount(new BigDecimal(overrideData.getIdd()
                                                                         .getVoicePool()).negate());
      dealElementFlist.set(FldUsageDiscount.getInst(),
          new BigDecimal(overrideData.getIdd().getVoicePool()).negate());
    }
    return refId;   
  }
}