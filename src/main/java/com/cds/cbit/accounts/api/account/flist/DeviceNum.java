package com.cds.cbit.accounts.api.account.flist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The POJO represents field mapping DEVICE_NUM section of BRM DEVICE_CREATE.
 * 
 * @author  Saibabu Guntur.
 * @version 1.0.
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class DeviceNum {

  @XmlElement(name = "CATEGORY_ID")
  private String categoryId;

  @XmlElement(name = "CATEGORY_VERSION")
  private String categoryVersion;
  
  @XmlElement(name = "NETWORK_ELEMENT")
  private String network;
} // End of DeviceNum.