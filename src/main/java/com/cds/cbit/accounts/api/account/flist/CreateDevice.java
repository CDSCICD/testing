package com.cds.cbit.accounts.api.account.flist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The POJO represents field mapping of BRM DEVICE_CREATE.
 * 
 * @author  Saibabu Guntur.
 * @version 1.0.
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "flist")
public class CreateDevice {

  @XmlElement(name = "PROGRAM_NAME")
  private String programName;

  @XmlElement(name = "POID")
  private String poid;
  
  @XmlElement(name = "DEVICE_ID")
  private String deviceId;
  
  @XmlElement(name = "STATE_ID")
  private String stateId;
  
  @XmlElement(name = "DEVICE_NUM")
  private DeviceNum deviceNum;
  
  @XmlElement(name = "DESCR")
  private String descr;
} // End of CreateDevice.