package com.cds.cbit.accounts.api.account.flist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The POJO represents field mapping of BRM DEVICE_SET_STATE.
 * 
 * @author  Saibabu Guntur.
 * @version 1.0.
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "flist")
public class DeviceSetState {

  @XmlElement(name = "POID")
  private String poid;

  @XmlElement(name = "NEW_STATE")
  private String newState;
} // End of DeviceSetState.