/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.validators;

import com.cds.cbit.accounts.api.account.flist.CreateDevice;
import com.cds.cbit.accounts.api.account.flist.DeviceNum;
import com.cds.cbit.accounts.api.account.flist.DeviceSetState;
import com.cds.cbit.accounts.commons.beans.Args;
import com.cds.cbit.accounts.commons.beans.Results;
import com.cds.cbit.accounts.commons.beans.Services;
import com.cds.cbit.accounts.exceptions.BillingException;
import com.cds.cbit.accounts.interfaces.MultiArgValidation;
import com.cds.cbit.accounts.util.BillingInfoSearchUtil;
import com.cds.cbit.accounts.util.BrmServiceUtil;
import com.cds.cbit.accounts.util.ConstantsUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalOp;
import com.portal.pcm.fields.FldDescr;
import com.portal.pcm.fields.FldPoid;
import com.portal.pcm.fields.FldResults;
import com.portal.pcm.fields.FldStateId;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Component;

/**
 * The implementation component provides methods for validating given  device inputs in the system.
 * The component overrides BillingValidationInterface validateInput method to create and execute 
 * device search FList.If the validation is success then the method will return output FList else
 * it will throw an exception to the caller.
 * 
 * @author  Venkata Nagaraju.
 * @version 1.0.
*/
@Log4j2
@Component("device")
public class DeviceValidateImpl implements MultiArgValidation {
  
  private final BillingInfoSearchUtil searchUtil;
  private final BrmServiceUtil brmServiceUtil;
  
  /**
   * Constructor injection.
   */
  public DeviceValidateImpl(final BillingInfoSearchUtil searchUtil,
                                                           final BrmServiceUtil brmServiceUtil) {
    
    this.searchUtil = searchUtil;
    this.brmServiceUtil = brmServiceUtil;
  } // End of constructor injection.
  
  /* @see com.circles.brm.api.v1.account.interfaces.BillingValidationInterface. */
  @Override
  public FList validateInput(final String input,final Object...data) 
                                                               throws EBufException, JAXBException {
    
    final String template = "sim".equalsIgnoreCase(String.valueOf(data[0]))
                                                ? ConstantsUtil.SIM_SQL : ConstantsUtil.MSISDN_SQL;
    
    return validateDevice(input,template,String.valueOf(data[1]),(PortalContext) data[2]);
  } // End of validateInput method.
  
  /**
   * The method will take device input like MSISDN,SIM and validate it in billing system.The method
   * creates Args and Results fields of search template and convert the search template to FList and
   * execute it in billing system with the help of BillingInfoSearchUtil.
   * 
   * @param: device   @Mandatory - MSISDN / SIM value .
   * @param: template @Mandatory - Search template [SQL Query of billing system].
  */
  public FList validateDevice(final String device,final String template, final String portin,
                                  final PortalContext portal) throws EBufException, JAXBException {
   
    final Args[] args = {Args.builder().elem("1").deviceId(device).build()};
    
    final Services services = Services.builder().elem("0")
                                                .accountObj(ConstantsUtil.ACCOUNT_POID).build();
    // Results field of search template.
    final Results results = Results.builder().elem("0").services(services).stateId("1")
                                                                          .description("").build();
    
    final FList msisdnOp = searchUtil.executeSearchTemplate(template,args,results);
    
    if ("true".equals(portin)) {
      if (msisdnOp.containsKey(FldResults.getInst()) && msisdnOp.get(FldResults.getInst())
                                               .getValues().get(0).get(FldStateId.getInst()) == 1) {
        throw new BillingException("200102");
      } 
      
      return portinProcess(msisdnOp,device,portal);
    }
    
    if (!(msisdnOp.hasField(FldResults.getInst()))) {
      throw new BillingException("100112");
    }
      
    final int status = msisdnOp.get(FldResults.getInst())
                                                .getValues().get(0).get(FldStateId.getInst());
    if (status != 1 && status != 4) {
      throw new BillingException("200101");
    }
    return msisdnOp;
  } // End of validating devices like MSISDN or SIM in BRM.
  
  private FList portinProcess(FList msisdnOp,String msisdn, PortalContext portal) 
                                                               throws EBufException, JAXBException {

    if (msisdnOp.hasField(FldResults.getInst()) && msisdnOp.get(FldResults.getInst()).getValues()
                                        .get(0).get(FldDescr.getInst()).toLowerCase().contains("lw")
        && msisdnOp.get(FldResults.getInst()).getValues().get(0).get(FldStateId.getInst()) == 10) {

      DeviceSetState deviceSetState = DeviceSetState.builder().newState("1")
                          .poid(String.valueOf(msisdnOp.get(FldResults.getInst()).getValues().get(0)
                                                                  .get(FldPoid.getInst()))).build();
      final JAXBContext jaxbContext = JAXBContext.newInstance(DeviceSetState.class);
      FList setStateFlist = brmServiceUtil.getFListFromPojo(jaxbContext,deviceSetState);
      log.info(setStateFlist);
      return portal.opcode(PortalOp.DEVICE_SET_STATE, setStateFlist);
      
    } else if (!msisdnOp.hasField(FldResults.getInst())) {
      DeviceNum deviceNum = DeviceNum.builder().categoryId("0").categoryVersion("0")
                                                                 .network("Non LW Network").build();
      CreateDevice createDevice = CreateDevice.builder().deviceNum(deviceNum)
                                     .poid(ConstantsUtil.DEVICE_POID).programName("Number Creation")
                                             .descr("Non-lw").stateId("1").deviceId(msisdn).build();
      final JAXBContext jaxbContext = JAXBContext.newInstance(CreateDevice.class);
      FList createDeviceFlist = brmServiceUtil.getFListFromPojo(jaxbContext,createDevice);
      log.info(createDeviceFlist);
      return portal.opcode(PortalOp.DEVICE_CREATE,createDeviceFlist);
    } else {
      throw new BillingException("200101");
    }
  } // End of portinProcess.
} // End DeviceValidateImpl class which validate device inputs.