/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.account.billdetail;

import com.cds.cbit.accounts.api.account.billdetail.payload.BillDetailRequest;
import com.cds.cbit.accounts.api.account.billdetail.payload.BillDetailResponse;
import com.cds.cbit.accounts.api.account.billdetail.payload.BillDetailResponseBody;
import com.cds.cbit.accounts.api.account.billdetail.payload.Result;
import com.cds.cbit.accounts.commons.beans.CustomerBillDetails;
import com.cds.cbit.accounts.commons.beans.ResponseHeader;
import com.cds.cbit.accounts.commons.beans.ResultInfo;
import com.cds.cbit.accounts.util.ResponseHeaderUtil;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.fields.FldAdjusted;
import com.portal.pcm.fields.FldBillNo;
import com.portal.pcm.fields.FldCurrentTotal;
import com.portal.pcm.fields.FldDisputed;
import com.portal.pcm.fields.FldDue;
import com.portal.pcm.fields.FldEndT;
import com.portal.pcm.fields.FldPreviousTotal;
import com.portal.pcm.fields.FldRecvd;
import com.portal.pcm.fields.FldResults;
import com.portal.pcm.fields.FldStartT;
import com.portal.pcm.fields.FldSysDescr;
import com.portal.pcm.fields.FldTransfered;
import com.portal.pcm.fields.FldWriteoff;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * The Component will provide method to prepare responses for all type of customer search like Bill,
 * Finance and Usage from the details retrieved from billing system.
 * 
 * @author Anuradha Manda.
 * @version 1.0
 * @author Meghashree Udupa.
 * @version 2.0
 *
 */
@Component
public class BillDetailsResponser {

  /**
   * The component class will prepare responses for all type of customer search like Bill, Finance
   * and Usage from the details retrieved from billing system.
   * 
   * @param- request
   * @param- outputFlist
   */
  public BillDetailResponse createBillDetailResponse(BillDetailRequest request, FList outputFlist)
      throws EBufException {
    String accountNo = request.getBody().getAccountNumber();

    List<CustomerBillDetails> billDetailsList = new ArrayList<>();

    Result results = new Result();
    results.setAccountNumber(accountNo);

    ResultInfo resultInfo = new ResultInfo();
    resultInfo.setResultCode("SUCCESS");
    resultInfo.setResultCodeId(0);

    if (!outputFlist.get(FldSysDescr.getInst()).equalsIgnoreCase("Data not found")
        && outputFlist.hasField(FldResults.getInst())) {
      List<FList> resultFlist = outputFlist.get(FldResults.getInst()).getValues();
      for (FList result : resultFlist) {
        CustomerBillDetails billDetails = new CustomerBillDetails();

        billDetails
            .setAdjusted(result.get(FldAdjusted.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails
            .setDisputed(result.get(FldDisputed.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails.setReceived(result.get(FldRecvd.getInst())
                                                         .setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails.setTransferred(
            result.get(FldTransfered.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails
            .setWriteOff(result.get(FldWriteoff.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails.setDue(result.get(FldDue.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails.setCurrentTotal(
            result.get(FldCurrentTotal.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails.setPrevTotal(
            result.get(FldPreviousTotal.getInst()).setScale(2, BigDecimal.ROUND_HALF_UP));
        billDetails.setBillNo(result.get(FldBillNo.getInst()));
        billDetails.setStartDate(
            String.valueOf(UtcDateUtil.convertDateToDateTime(result.get(FldStartT.getInst()))));
        billDetails.setEndDate(
            String.valueOf(UtcDateUtil.convertDateToDateTime(result.get(FldEndT.getInst()))));
        billDetailsList.add(billDetails);
      }

      results.setBillDetails(billDetailsList);
      resultInfo.setResultMsg("Bills fetched successfully.");

    } else {
      resultInfo.setResultMsg("Bill details not found for the given dates.");
    }

    BillDetailResponseBody responseBody = BillDetailResponseBody.builder().result(results)
        .resultInfo(resultInfo).build();
    final ResponseHeader header = ResponseHeaderUtil.createResponseHead(request.getHead());

    return BillDetailResponse.builder().head(header).body(responseBody).build();
  } // End of createBillDetail method.
} // End of BillDetailsResponser class.