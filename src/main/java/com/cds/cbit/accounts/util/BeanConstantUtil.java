/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.util;

/**
 * The util class will define all the bean names as constants required for account services.
 * 
 * @author Saibabu Guntur
 * @Version 1.0.
*/
public final class BeanConstantUtil {
  
  public static final String ACCT_DETAILS = "AccountDetails";
  
  private BeanConstantUtil() {
  } // End of private constructor.
} // End of BeanConstantUtil.