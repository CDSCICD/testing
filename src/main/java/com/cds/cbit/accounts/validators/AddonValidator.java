package com.cds.cbit.accounts.validators;

import com.cds.cbit.accounts.api.customer.detail.flist.ExtraResults;
import com.cds.cbit.accounts.api.customer.detail.flist.LinkedObj;
import com.cds.cbit.accounts.api.customer.detail.flist.ResultsDynamo;
import com.cds.cbit.accounts.commons.beans.Args;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.BillingInfoSearchUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;

import java.math.BigDecimal;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Component;


/**
 * The implementation component provides methods for validating account for ADDON purchase 
 *  in the system.
 * The component overrides BillingValidationInterface validateInput method to create and execute 
 * add on search FList.If the validation is success then the method will return output FList else
 * it will throw an exception to the caller.
 * 
 * @author Meghashree Udupa
 *
 */

@Component("addonValidator")
public class AddonValidator implements BillingValidation {

  private final BillingInfoSearchUtil searchUtil;

  public AddonValidator(BillingInfoSearchUtil searchUtil) {
    this.searchUtil = searchUtil;
  }

  @Override
  public FList validateInput(String input, String... data) throws EBufException, JAXBException {

    ResultsDynamo results = ResultsDynamo.builder().elem("0").packageId(String.valueOf(0))
            .planObj("").dealObj("").purchaseEndT(0L).purchaseStartT(0L).planObj("").createdT(0L)
                        .linkedObj(LinkedObj.builder().elem("2").linkDirection("-1").dealObj("")
                       .extraResults(ExtraResults.builder().elem("0").build()).build())
                        .cycleDiscount(new BigDecimal(0)).cycleDisAmt(new BigDecimal(0))
                        .cycleEndDetails(0)
                        .cycleFeeAmt(new BigDecimal(0)).cycleFeeFlags(0).cycleStartDetails(0)
                        .priceListName("").purchaseDiscount(new BigDecimal(0))
                        .purchaseDiscAmt(new BigDecimal(0))
                        .purchaseEndDetails(0).purchaseFeeAmt(new BigDecimal(0))
                        .purchaseStartDetails(0)
                        .usageDiscount(new BigDecimal(0)).usageEndDetails(0).usageStartDetails(0)
                        .endT("").build();

    Args dealObj = Args.builder().elem("1").dealObj("").build();
    Args poidArgs = Args.builder().elem("2").poid("").build();
    Args accountObj = Args.builder().elem("3").accountObj(input).build();
    Args acctNoArgs = Args.builder().elem("4").status("3").build();

    Args[] arguments = { dealObj, poidArgs, accountObj, acctNoArgs };

    String addOnTemplate = "select X from  /purchased_product 1 , /deal 2 where 1.F1=2.F2 "
        + "and 1.F3=V3 and 1.F4 != V4";

    return searchUtil.executeSearchTemplateDynamo(addOnTemplate, arguments, results, "1280");
  } // End of validateInput method.
} // End of AddonValidator class.
