/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.customer.detail;

import com.cds.cbit.accounts.api.customer.detail.payload.AccountInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.CustomerDetailResponse;
import com.cds.cbit.accounts.api.customer.detail.payload.CustomerDetailsRequest;
import com.cds.cbit.accounts.api.customer.detail.payload.CustomerDetailsResponseBody;
import com.cds.cbit.accounts.api.customer.detail.payload.OrderInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.PaymentInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.PersonalInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.PlanInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.PortinInfo;
import com.cds.cbit.accounts.api.customer.detail.payload.Result;
import com.cds.cbit.accounts.commons.beans.ResponseHeader;
import com.cds.cbit.accounts.commons.beans.ResultInfo;
import com.cds.cbit.accounts.factory.BillingValidationFactory;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.ResponseHeaderUtil;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.fields.FldDeviceId;
import com.portal.pcm.fields.FldResults;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.JAXBException;
import lombok.extern.log4j.Log4j2;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

/**
 * The component will help to prepare the response for customer detail API,
 * It will prepare all the details related to profile information,
 * order information, name information, address information product information ,
 * add on information of the customer from billing system .
 * 
 * @author Meghashree Udupa.
 * @version 1.0
 */
@Log4j2
@Component
public class CustomerDetailResponser {

  private final BillingValidationFactory factory;
  private final CustomerDetailHelper responseHelper;

  private static final String DEFAULT = "1970-01-01T00:00:00Z";
  
  final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
  
  /** Constructor Injection. */
  public CustomerDetailResponser(final BillingValidationFactory factory, 
                                 final CustomerDetailHelper responseHelper) {
    this.responseHelper = responseHelper;
    this.factory = factory;
  } // End of Constructor injection.
  
  /**
   * The method will prepare the response for customer detail API.It will prepare all the details 
   * related to profile,order,name,address,product and add on information of the customer 
   * from billing system.
   * 
   * @throws- ParseException 
   * @param- request
   * @param- responseDetails
   */
  public CustomerDetailResponse processCustomerDetailResponse(
         final CustomerDetailsRequest request,final Map<String, Object> responseDetails,
                                      final Poid accountPoid,final Date creationDate) 
                                         throws EBufException,JAXBException, ParseException {

    final PlanInfo planInfo = responseHelper.getPlanDetails(responseDetails, accountPoid);

    final Map<String, Object> infoMap = responseHelper.getProfileDetail(responseDetails);
    final PersonalInfo personalInfo = (PersonalInfo) infoMap.get("personalInfo");
    final PaymentInfo paymentInfo = (PaymentInfo) infoMap.get("paymentInfo");
    final OrderInfo orderInfo = (OrderInfo) infoMap.get("orderInfo");
    
    final BillingValidation accountDeviceValidator = factory.getValidator("accountDeviceValidator");
    final FList msisdnFlist = accountDeviceValidator.validateInput(String.valueOf(accountPoid));
    
    String msisdn = "";
    if (msisdnFlist.hasField(FldResults.getInst())) {
      msisdn = msisdnFlist.get(FldResults.getInst()).getValues().get(0).get(FldDeviceId.getInst());
    }
    
    final String autoBoost = personalInfo.getAutoBoost();
    final String autoRenewFlag = personalInfo.getAutoRenewFlag();
    final String category = personalInfo.getCategory();
    final String segment = personalInfo.getSegment();
    final String accountType = personalInfo.getAccountType();
    final String invoiceLangPref = personalInfo.getInvoiceLangPref();
    final String billingDate = personalInfo.getBillingDate();
    final String subType = personalInfo.getSubType();
    final String custConsent = personalInfo.getCustConsent();
    final String locale = personalInfo.getLocale();
    
    final Map<String, Object> portinInfoMap = responseHelper.getPortinInfoDetail(responseDetails);
    PortinInfo portinInfo = null;
    if (portinInfoMap.size() > 0) {
      portinInfo = PortinInfo.builder()
                             .date(String.valueOf(portinInfoMap.get("portinDate")))
                             .donor(String.valueOf(portinInfoMap.get("portinDonor")))
                             .msisdn(String.valueOf(portinInfoMap.get("portinNumber"))).build();
    }
     
    String planChangeDate = DEFAULT;
    String activeDate = getPlanActivationDate(paymentInfo,planInfo);
    
    if (activeDate == null) {
      activeDate = DEFAULT;
    }
    
    if (paymentInfo.getPlanChangeDate() != null) {
      planChangeDate = paymentInfo.getPlanChangeDate();
    }
    
    planInfo.setActivationDate(activeDate);
    planInfo.setPlanChangeDate(planChangeDate);
    
    String accountCreatedDate = dateFormatter.print(UtcDateUtil
                                                             .convertDateToDateTime(creationDate));
    
    final Result result = Result.builder().accountInfo(AccountInfo.builder()
                .accountNumber(request.getBody().getAccountNumber()).autoBoost(autoBoost)
                .msisdn(msisdn).category(category).segment(segment).portinInfo(portinInfo)
                .accountType(accountType).invoiceLangPref(invoiceLangPref).billingDate(billingDate)
                .subType(subType).custConsent(custConsent).autoRenewFlag(autoRenewFlag)
                .locale(locale).accountCreatedDate(accountCreatedDate).build())
                .personalInfo(personalInfo)
                .planInfo(planInfo).paymentInfo(paymentInfo).orderInfo(orderInfo).build();

    final ResultInfo resultInfo = 
          ResultInfo.builder().resultCode("SUCCESS").resultCodeId(0)
                                        .resultMsg("Cutomer details fetched successfully").build();
    final CustomerDetailsResponseBody body = 
               CustomerDetailsResponseBody.builder().result(result).resultInfo(resultInfo).build();
    
    log.info(body.toString());
    final ResponseHeader header = ResponseHeaderUtil.createResponseHead(request.getHead());
    
    return CustomerDetailResponse.builder().body(body).head(header).build();
  } // End of processCustomerDetailResponse method.

  /** Method to get plan activation date. **/
  private String getPlanActivationDate(PaymentInfo paymentInfo, PlanInfo planInfo) 
                                                                        throws ParseException {
    String activeDate = null;
    final SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    if (planInfo.getPurchaseTimestamp() == 0) {
      activeDate = planInfo.getPurchaseStartTime();
      log.info("getPurchaseTimestamp {}", activeDate);
    } else if (paymentInfo.getPlanActDate() != null) {
      Date activation = UtcDateUtil.convertStringToDate(paymentInfo.getPlanActDate());
      activeDate = simpleDate.format(activation);
      log.info("getPlanActDate{} ", activeDate);
    } else {
      activeDate = planInfo.getPurchaseStartTime();
      log.info("getPurchaseStartTime{} ", activeDate);
    }
    log.info("return {}", activeDate);
    return activeDate;
  }
} // End of CustomerDetailResponser class.