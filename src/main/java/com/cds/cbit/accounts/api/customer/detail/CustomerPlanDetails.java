/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.customer.detail;

import com.cds.cbit.accounts.api.account.payload.DiscountDetails;
import com.cds.cbit.accounts.api.account.payload.OverrideDetail;
import com.cds.cbit.accounts.api.customer.detail.payload.Addons;
import com.cds.cbit.accounts.api.customer.detail.payload.PlanInfo;
import com.cds.cbit.accounts.factory.BillingValidationFactory;
import com.cds.cbit.accounts.interfaces.BillingValidation;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.fields.FldCode;
import com.portal.pcm.fields.FldCreatedT;
import com.portal.pcm.fields.FldCycleDiscAmt;
import com.portal.pcm.fields.FldCycleDiscount;
import com.portal.pcm.fields.FldDealObj;
import com.portal.pcm.fields.FldDescr;
import com.portal.pcm.fields.FldExtraResults;
import com.portal.pcm.fields.FldName;
import com.portal.pcm.fields.FldPackageId;
import com.portal.pcm.fields.FldPoid;
import com.portal.pcm.fields.FldPriceListName;
import com.portal.pcm.fields.FldPurchaseDiscAmt;
import com.portal.pcm.fields.FldPurchaseDiscount;
import com.portal.pcm.fields.FldPurchaseEndT;
import com.portal.pcm.fields.FldPurchaseStartT;
import com.portal.pcm.fields.FldResults;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import lombok.extern.log4j.Log4j2;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The component will provide methods to retrieve customer base plan and addOn plan details
 * as part of GetCustomerDetails API response.
 * 
 * @author  Meghashree udupa, Saibabu Guntur.
 * @version 2.0.
*/
@Log4j2
@Component
public class CustomerPlanDetails {
  
  @Autowired
  private BillingValidationFactory factory;
  

  final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

  /** Method to prepare ADDON details for the customer from billing system. */
  protected PlanInfo getAddonDetail(
            final Map<String, Object> responseDetails,final Poid accountPoid)
                                                            throws EBufException, JAXBException {

    final FList productDetail = (FList) responseDetails.get("productDetail");
    
    final List<Addons> addonList = new ArrayList<>();
    
    final List<String> packagesList = new ArrayList<>();

    if (productDetail.hasField(FldResults.getInst())) {

      for (int i = 0; i < productDetail.get(FldResults.getInst()).getValues().size(); i++) {
        final Addons addons = new Addons();
        final long dealObj = productDetail.get(FldResults.getInst()).getValues().get(i)
                                                                .get(FldDealObj.getInst()).getId();
        final BillingValidation dealsValidator = factory.getValidator("dealValidator");
        final FList dealsDetail = dealsValidator.validateInput(String.valueOf(
            productDetail.get(FldResults.getInst()).getValues().get(i).get(FldDealObj.getInst())));

        if (dealsDetail.hasField(FldResults.getInst())) {
          continue;
        }
        long purchaseEnd = productDetail.get(FldResults.getInst()).getValues().get(i)
                                                         .get(FldPurchaseEndT.getInst()).getTime();
        if (new DateTime(purchaseEnd).getYear() != 1970) {
          purchaseEnd = purchaseEnd - 1000;
        }
        
        String purchaseEndTime = formatter.print(UtcDateUtil
                                                    .convertDateToDateTime(new Date(purchaseEnd)));
        addons.setEndDate(purchaseEndTime);
        addons.setExpiryDate(purchaseEndTime);
        
        final Date prchaseStart = productDetail.get(FldResults.getInst()).getValues().get(i)
                                                                 .get(FldPurchaseStartT.getInst());
        final DateTime prchaseTime = UtcDateUtil.convertDateToDateTime(prchaseStart);
        final String prchaseStartTime = formatter.print(prchaseTime);

        addons.setStartDate(prchaseStartTime);
        addons.setActivationDate(prchaseStartTime);
        
        addons.setPackageId(productDetail.get(FldResults.getInst()).getValues().get(i)
                                                          .get(FldPackageId.getInst()).toString());
        packagesList.add(productDetail.get(FldResults.getInst()).getValues().get(i)
                                                          .get(FldPackageId.getInst()).toString());
        Long endDynamo = 0L;
        try {
          if (!productDetail.get(FldResults.getInst()).getValues().get(i)
                                                             .get(FldDescr.getInst()).isEmpty()) {
            endDynamo = Long.valueOf(productDetail.get(FldResults.getInst()).getValues()
                                                                  .get(i).get(FldDescr.getInst()));
          }
        } catch (NumberFormatException e) {
          log.info("-----------Discount expired---------");
        }

        endDynamo = endDynamo * 1000;
        if (new DateTime(endDynamo).getYear() != 1970) {
          endDynamo = endDynamo - 1000;
        }
        
        final String dynamicEndTimeCon = formatter.print(UtcDateUtil
                                                      .convertDateToDateTime(new Date(endDynamo)));
       
        Date dynamicStartT = productDetail.get(FldResults.getInst()).getValues()
                                                              .get(i).get(FldCreatedT.getInst());
        final DateTime dynamicStartTime = UtcDateUtil.convertDateToDateTime(dynamicStartT);
        final String dynamicStartTimeCon = formatter.print(dynamicStartTime);
        
        final FList dealDetails = productDetail.get(FldResults.getInst()).getValues().get(i);
        DiscountDetails discountDetail = DiscountDetails.builder()
                                                        .startDate(dynamicStartTimeCon)
                                                        .endDate(dynamicEndTimeCon)
                                                        .build();
        
        if (dealDetails.hasField(FldCycleDiscAmt.getInst()) 
               && dealDetails.get(FldCycleDiscAmt.getInst()).compareTo(BigDecimal.ZERO) > 0) {
          discountDetail.setAmount(String.valueOf(dealDetails.get(FldCycleDiscAmt.getInst())));
        }
        
        if (dealDetails.hasField(FldCycleDiscount.getInst()) 
              && dealDetails.get(FldCycleDiscount.getInst()).compareTo(BigDecimal.ZERO) > 0) {
          discountDetail.setPercentage(String.valueOf(dealDetails
                             .get(FldCycleDiscount.getInst()).multiply(new BigDecimal(100))));
        }
        
        if (dealDetails.hasField(FldPurchaseDiscAmt.getInst()) 
             && dealDetails.get(FldPurchaseDiscAmt.getInst()).compareTo(BigDecimal.ZERO) > 0) {
          discountDetail.setAmount(String.valueOf(dealDetails.get(FldPurchaseDiscAmt.getInst())));
        }
        
        if (dealDetails.hasField(FldPurchaseDiscount.getInst()) 
            && dealDetails.get(FldPurchaseDiscount.getInst()).compareTo(BigDecimal.ZERO) > 0) {
          discountDetail.setPercentage(String.valueOf(dealDetails.get(FldPurchaseDiscount.getInst())
                                                                   .multiply(new BigDecimal(100))));
        }
        
        if ((discountDetail.getAmount() != null && new BigDecimal(discountDetail.getAmount())
             .compareTo(BigDecimal.ZERO) > 0) || (discountDetail.getPercentage() != null 
             && new BigDecimal(discountDetail.getPercentage()).compareTo(BigDecimal.ZERO) > 0)) {
          addons.setDiscountDetail(discountDetail);
        }
        
        OverrideDetail overrideDetail = OverrideDetail.builder().refId(productDetail
                .get(FldResults.getInst()).getValues().get(i)
                .get(FldPriceListName.getInst()))
                .build();
        if (overrideDetail.getRefId() != null && !overrideDetail.getRefId().isEmpty()) {
          addons.setOverrideDetail(overrideDetail);
        }
       
        for (int k = 0; k < productDetail.get(FldExtraResults.getInst()).getValues().size(); k++) {
          final long dealPoid = 
                     productDetail.get(FldExtraResults.getInst()).getValues().get(k)
                                                                 .get(FldPoid.getInst()).getId();
          if (dealObj == dealPoid) {
            addons.setProduct(productDetail.get(FldExtraResults.getInst()).getValues().get(k)
                                                                          .get(FldName.getInst()));
            discountDetail.setDealCode(productDetail.get(FldExtraResults.getInst())
                                                       .getValues().get(k).get(FldCode.getInst()));
          }
        }
        addonList.add(addons);
      }
    }

    final PlanInfo planInfo = getPlanInformation(accountPoid,formatter);
    
    planInfo.setAddons(addonList);
    return planInfo;
  } // End of getAddonDetail method.
  
  /** Method to prepare basic plan details from billing system. */
  private PlanInfo getPlanInformation(final Poid accountPoid,final DateTimeFormatter formatter) 
                                                                throws EBufException,JAXBException {
    final StringBuilder createdDate = new StringBuilder();
    
    final StringBuilder packageId = new StringBuilder();
    final StringBuilder baseplanName = new StringBuilder();
    final StringBuilder purchaseStart = new StringBuilder();
    Long purchaseTimestamp = 0L;
    
    final BillingValidation basicPlanValidator = factory.getValidator("basicPlanValidator");
    final FList baseplanOp = basicPlanValidator.validateInput(String
                                                        .valueOf(accountPoid),"withoutOD");
    DiscountDetails discountDetail = new DiscountDetails();
    OverrideDetail overrideDetail = new OverrideDetail();
    
    if (baseplanOp.hasField(FldResults.getInst())) {
      final FList basePlanResult = baseplanOp.get(FldResults.getInst()).getValues().get(0);
      
      final DateTime createdTime = 
            UtcDateUtil.convertDateToDateTime(basePlanResult.get(FldCreatedT.getInst()));
      createdDate.append(formatter.print(createdTime));
      packageId.append(String.valueOf(basePlanResult.get(FldPackageId.getInst())));
      baseplanName.append(baseplanOp.get(FldExtraResults.getInst())
                                    .getValues().get(0).get(FldName.getInst()));
      
      final DateTime purchaseTime =  UtcDateUtil.convertDateToDateTime(basePlanResult
                                                           .get(FldPurchaseStartT.getInst()));
      purchaseTimestamp = basePlanResult.get(FldPurchaseStartT.getInst()).getTime();
      purchaseStart.append(formatter.print(purchaseTime));
      
      setDynamicAndOverrideDetails(discountDetail,overrideDetail,String.valueOf(accountPoid));
      
    } else {
      createdDate.append("1970-01-01T00:00:00Z");
    }

    PlanInfo planInfo = PlanInfo.builder().startDate(createdDate.toString())
                             .name(baseplanName.toString()).packageId(packageId.toString())
                             .purchaseStartTime(purchaseStart.toString())
                             .purchaseTimestamp(purchaseTimestamp)
                             .build();

    if ((discountDetail.getAmount() != null && new BigDecimal(discountDetail.getAmount())
          .compareTo(BigDecimal.ZERO) > 0) || (discountDetail.getPercentage() != null 
          && new BigDecimal(discountDetail.getPercentage()).compareTo(BigDecimal.ZERO) > 0)) {
      planInfo.setDiscountDetail(discountDetail);
    }
    
    if (overrideDetail.getRefId() != null && !overrideDetail.getRefId().isEmpty()) {
      planInfo.setOverrideDetail(overrideDetail);
    }
    return planInfo;
  } // End of getPlanInformation method.

  private void setDynamicAndOverrideDetails(DiscountDetails discountDetail,
                                              OverrideDetail overrideDetail,String accountPoid) 
                                                           throws EBufException, JAXBException {
    
    final BillingValidation basicPlanValidator = factory.getValidator("basicPlanValidator");
    final FList baseplanOp = basicPlanValidator.validateInput(String
                                                        .valueOf(accountPoid),"withOverride");
    if (baseplanOp.hasField(FldResults.getInst())) {
      for (int i = 0;i < baseplanOp.get(FldResults.getInst()).size();i++) {
        final FList basePlanResult = baseplanOp.get(FldResults.getInst()).getValues().get(i);
        Long endDynamo = 0L;
        if (!basePlanResult.get(FldDescr.getInst()).isEmpty() 
                    && !basePlanResult.get(FldDescr.getInst()).equals("Discount_expired")) {
          endDynamo = Long.valueOf(basePlanResult.get(FldDescr.getInst())) * 1000; 
        }
        if (new DateTime(endDynamo).getYear() != 1970) {
          endDynamo = endDynamo - 1000;
        }
        final String dynamicEndTimeCon = formatter.print(UtcDateUtil
                .convertDateToDateTime(new Date(endDynamo)));

        Date dynamicStartT = basePlanResult.get(FldCreatedT.getInst());
        final DateTime dynamicStartTime = UtcDateUtil.convertDateToDateTime(dynamicStartT);
        final String dynamicStartTimeCon = formatter.print(dynamicStartTime);
    
        if (basePlanResult.get(FldCycleDiscAmt.getInst()).compareTo(BigDecimal.ZERO) > 0
             || basePlanResult.get(FldCycleDiscount.getInst()).compareTo(BigDecimal.ZERO) > 0) {
          discountDetail.setDealCode(baseplanOp.get(FldExtraResults.getInst())
                                                    .getValues().get(0).get(FldCode.getInst()));
          if (basePlanResult.hasField(FldCycleDiscAmt.getInst()) 
              && basePlanResult.get(FldCycleDiscAmt.getInst()).compareTo(BigDecimal.ZERO) > 0) {
            discountDetail.setAmount(String.valueOf(basePlanResult.get(FldCycleDiscAmt.getInst())));
          }

          if (basePlanResult.hasField(FldCycleDiscount.getInst()) 
                 && basePlanResult.get(FldCycleDiscount.getInst()).compareTo(BigDecimal.ZERO) > 0) {
            discountDetail.setPercentage(String.valueOf(basePlanResult
                                  .get(FldCycleDiscount.getInst()).multiply(new BigDecimal(100))));
          }
          if (basePlanResult.hasField(FldPurchaseDiscAmt.getInst()) 
               && basePlanResult.get(FldPurchaseDiscAmt.getInst()).compareTo(BigDecimal.ZERO) > 0) {
            discountDetail.setAmount(String.valueOf(basePlanResult
                                                               .get(FldPurchaseDiscAmt.getInst())));
          }

          if (basePlanResult.hasField(FldPurchaseDiscount.getInst()) 
              && basePlanResult.get(FldPurchaseDiscount.getInst()).compareTo(BigDecimal.ZERO) > 0) {
            discountDetail.setPercentage(String.valueOf(basePlanResult
                                .get(FldPurchaseDiscount.getInst()).multiply(new BigDecimal(100))));
          }
          discountDetail.setEndDate(dynamicEndTimeCon);
          discountDetail.setStartDate(dynamicStartTimeCon);    
        }

        if (basePlanResult.get(FldPriceListName.getInst()) != null) {
          overrideDetail.setRefId(basePlanResult.get(FldPriceListName.getInst()));
        }
      }
    }
  }
} // End of CustomerPlanDetails, which fetch customer plan details.