/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.validators;

import com.cds.cbit.accounts.api.account.balance.flists.BalImpact;
import com.cds.cbit.accounts.aspects.Loggable;
import com.cds.cbit.accounts.commons.beans.Args;
import com.cds.cbit.accounts.commons.beans.QuantityTires;
import com.cds.cbit.accounts.commons.beans.Results;
import com.cds.cbit.accounts.util.BillingInfoSearchUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Component;

/**
 * The implementation component provides methods for validating given account inputs in the system.
 * The component overrides BillingValidationInterface validateInput method to create and execute
 * account search FList.If the validation is success then the method will return output FList else
 * it will throw an exception to the caller.
 * 
 * @author Meghashree Udupa.
 * @version 1.0.
 */
@Component("productCurrencyValidator")
public class ProductCurrencyValidator {

  private final BillingInfoSearchUtil searchUtil;

  /** Constructor injection. **/
  public ProductCurrencyValidator(final BillingInfoSearchUtil searchUtil) {

    this.searchUtil = searchUtil;
  } // End of Constructor Injection

  /** Method to validate currency and non currency resources . */
  @Loggable
  public FList validateInput(final String productPoid, final int resourceId, final String... data) 
                                                             throws EBufException, JAXBException {
   
    List<BalImpact> balImpacts1 = new ArrayList<>();
    BalImpact bal1 = BalImpact.builder().elem("*").elementId(0).build();
    balImpacts1.add(bal1);
    
    List<QuantityTires> quantityList1 = new ArrayList<>();
    QuantityTires quant1 = QuantityTires.builder().elem("0").balImpacts(balImpacts1).build();
    quantityList1.add(quant1);
    
    final Results results = Results.builder().elem("0").quantityTires(quantityList1)
        .build();
    // Preparing Args section of PCM_OP_SEARCH.

    final Args poidArgs = Args.builder().elem("1").poid("0.0.0.1 /product 0").build();
    final Args prodArgs = Args.builder().elem("2").productObj("0.0.0.1 /product 0").build();
    final Args productPoidArgs = Args.builder().elem("3").poid(productPoid).build();
    final Args planObjArgs = Args.builder().elem("4").ratePlanObj("0.0.0.1 /rate_plan 0").build();
    final Args planPoidArgs = Args.builder().elem("5").poid("0.0.0.1 /rate_plan 0").build();
    
    List<BalImpact> balImpacts = new ArrayList<>();
    BalImpact bal = BalImpact.builder().elementId(resourceId).build();
    balImpacts.add(bal);
    
    List<QuantityTires> quantityList = new ArrayList<>();
    QuantityTires quant = QuantityTires.builder().elem("0").balImpacts(balImpacts).build();
    quantityList.add(quant);
    final Args quantityTiers = Args.builder().elem("6").quantityTires(quantityList).build();

    final Args[] arguments = { poidArgs, prodArgs, productPoidArgs, 
        planObjArgs, planPoidArgs, quantityTiers };

    final StringBuilder tempelate = new StringBuilder("select X from /rate 1, /product 2, "
        + "/rate_plan 3 where 2.F1 = 3.F2 and 2.F3 = V3 and 1.F4 = 3.F5 and 1.F6 = V6");

    
    return searchUtil.executeSearchTemplate(tempelate.toString(), arguments, results);
  } // End of validating given account number in BRM.
} // End of ProductCurrencyValidator.