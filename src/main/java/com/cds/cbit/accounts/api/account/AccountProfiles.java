/*
 * Copyright (C) 2019 Covalensedigital 
 *
 * Licensed under the CBIT,Version 1.0,you may not use this file except in compliance with the 
 * License. You may obtain a copy of the License at 
 * 
 * http://www.covalensedigital.com/
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or
 * implied.See the License for the specific language governing permissions and limitations under.
*/

package com.cds.cbit.accounts.api.account;

import com.cds.cbit.accounts.api.account.flist.BalanceInfo;
import com.cds.cbit.accounts.api.account.flist.Deal;
import com.cds.cbit.accounts.api.account.flist.Devices;
import com.cds.cbit.accounts.api.account.flist.Limits;
import com.cds.cbit.accounts.api.account.flist.Services;
import com.cds.cbit.accounts.api.account.payload.AccountInfo;
import com.cds.cbit.accounts.api.account.payload.AccountRequestBody;
import com.cds.cbit.accounts.api.account.payload.Identity;
import com.cds.cbit.accounts.api.account.payload.OrderInfo;
import com.cds.cbit.accounts.api.account.payload.PaymentInfo;
import com.cds.cbit.accounts.api.account.payload.PersonalInfo;
import com.cds.cbit.accounts.api.account.payload.PortinInfo;
import com.cds.cbit.accounts.util.BrmServiceUtil;
import com.cds.cbit.accounts.util.UtcDateUtil;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.PortalOp;
import com.portal.pcm.fields.FldBalInfoIndex;
import com.portal.pcm.fields.FldCreditFloor;
import com.portal.pcm.fields.FldCreditLimit;
import com.portal.pcm.fields.FldCreditThresholds;
import com.portal.pcm.fields.FldCreditThresholdsFixed;
import com.portal.pcm.fields.FldDealObj;
import com.portal.pcm.fields.FldDeals;
import com.portal.pcm.fields.FldLimit;
import com.portal.pcm.fields.FldPoid;
import com.portal.pcm.fields.FldServiceId;
import com.portal.pcm.fields.FldServiceObj;
import com.portal.pcm.fields.FldServices;
import com.portal.pcm.fields.FldSubscriptionIndex;
import com.portal.pcm.fields.FldVirtualT;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The component provide methods which will create various information required for profile section
 * of CUST_COMMIT opcode. These include, KYC, portin, payment related information and so on.
 * 
 * @author  Venkata Nagaraju.
 * @version 1.0.
*/
@Component
public class AccountProfiles {
   
  @Autowired
  BrmServiceUtil serviceUtil;
  
  final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
  private static final String DEFAULT = "1970-01-01T00:00:00Z";
   
  /** 
   * The method will create customer profile information with the given input in the request. The
   * method uses internal methods to gather complete information required for profile creation.
   * 
   * @param: account -  @Mandatory - Account create request body section.
  */
  public Map<String, String> createAccountProfiles(final AccountRequestBody account,
                                                        String planType) throws EBufException {
    
    final Map<String,String> profiles = new ConcurrentHashMap<>();
    final AccountInfo accountInfo = account.getAccountInfo();
    profiles.putAll(
             createProfileInfo(account.getPersonalInfo(),account.getPaymentInfo(),
                                                   account.getOrderInfo(),accountInfo,planType));
    
    Optional.ofNullable(accountInfo.getPortinInfo())
                                 .ifPresent(p ->  profiles.putAll(getPortinProfile(p)));
    return profiles;
  } // End of creating profile section of CUST_COMMIT_OPCODE.
   
  /**
   * The method will create and return a map which contains various fields need to keep in profiles.
   * 
   * @param: personal-    @Mandatory - Customer personal information.
   * @param: payment-     @Mandatory - Customer payment details.
   * @param: order-       @Mandatory - Customer order information.
   * @param: accountInfo- @Mandatory - Customer account details.
  */
  public Map<String, String> 
         createProfileInfo(final PersonalInfo personal,final PaymentInfo payment,
                          final OrderInfo order,final AccountInfo accountInfo,String planType) 
                                                                        throws EBufException {
    
    final Map<String,String> profiles = new ConcurrentHashMap<>();
    
    
    String activationDate = "";
    String planChangeDate = "";
   
    
    if ("normalPlan".equals(planType)) {
      activationDate = dateFormatter.print(getPvtTime());
      planChangeDate = DEFAULT;
    } else {
      activationDate = DEFAULT;
      planChangeDate = DEFAULT;
    }
    
    profiles.put("planActDate_planInfo", activationDate);
    profiles.put("planChangeDate_planInfo", planChangeDate);
    
    
    profiles.put("dateOfBirth_personalInfo", personal.getDateOfBirth());
    profiles.put("nationality_personalInfo", personal.getNationality());
    profiles.put("autoBoost_accountInfo",   accountInfo.getAutoBoost());
    
    if (accountInfo.getAutoRenewFlag() != null && !accountInfo.getAutoRenewFlag().isEmpty()) {
      profiles.put("autoRenewFlag_accountInfo",   accountInfo.getAutoRenewFlag());
    }
    
    if (accountInfo.getLocale() != null && !accountInfo.getLocale().isEmpty()) {
      profiles.put("locale_accountInfo", accountInfo.getLocale());
    }
    
    profiles.put("subType_accountInfo",     accountInfo.getSubType());
    profiles.put("category_accountInfo",    accountInfo.getCategory());
    profiles.put("orderNumber_orderInfo", order.getNumber());
    profiles.put("checkoutDate_orderInfo",order.getCheckoutDate());
    
    if (order.getReferralCode() != null && !order.getReferralCode().isEmpty()) {
      profiles.put("referralCode_orderInfo",order.getReferralCode());
    }
    
    profiles.putAll(getStreetAddress(personal));
    profiles.putAll(getOptionalFields(personal,accountInfo));
    
    profiles.put("msisdn_accountInfo", accountInfo.getMsisdn());
    
    if (accountInfo.getPortinInfo() != null) {
      profiles.put("portinNumber_portinInfo",   accountInfo.getPortinInfo().getMsisdn());
      profiles.put("portinDate_portinInfo",   accountInfo.getPortinInfo().getDate());
      profiles.put("portinDonor_portinInfo",   accountInfo.getPortinInfo().getDonor());
    }
    Optional.ofNullable(payment).ifPresent(p ->  profiles.putAll(getPaymentProfile(p)));
    
    return profiles;
  } // End of generic profile creation.
  
  /** Method to create profile section optional fields. **/
  private Map<String, String> getOptionalFields(
                              final PersonalInfo personal,final AccountInfo accountInfo) {
    
    final Map<String,String> profiles = new ConcurrentHashMap<>();
    
    profiles.put("accountType_accountInfo",accountInfo.getAccountType());
    profiles.put("invoiceLangPref_accountInfo",accountInfo.getInvoiceLangPref());
    if (accountInfo.getSegment() != null && !accountInfo.getSegment().isEmpty()) {
      profiles.put("segment_accountInfo",accountInfo.getSegment());
    }
    if (personal.getAltContact() != null && !personal.getAltContact().isEmpty()) {
      profiles.put("altContact_personalInfo",personal.getAltContact());
    }
    profiles.put("custConsent_accountInfo",accountInfo.getCustConsent());
    profiles.values().removeIf(Objects::isNull);
    return profiles;
  } // End of creating optional fields of profile section.
  
  /** Method to Create street address. **/
  private Map<String, String> getStreetAddress(final PersonalInfo personal) {

    final Map<String,String> profiles = new ConcurrentHashMap<>();
    
    if (personal.getAddress().getBuildingNo() != null 
                       && !personal.getAddress().getBuildingNo().isEmpty()) {
      profiles.put("buildingNo_address", personal.getAddress().getBuildingNo());
    }
    
    if (personal.getAddress().getFloor() != null 
                           && !personal.getAddress().getFloor().isEmpty()) {
      profiles.put("floor_address", personal.getAddress().getFloor());
    }
    
    if (personal.getAddress().getUnit() != null 
                           && !personal.getAddress().getUnit().isEmpty()) {
      profiles.put("unit_address", personal.getAddress().getUnit());
    }
    
    return profiles;
  } // End of getStreetAddress.
  
  /** Method to create KYC profile. **/
  public Map<String, String> getKycProfile(final List<Identity> identity) {
    
    final Map<String,String> idMap = new ConcurrentHashMap<>();
    
    // Changes done here to convert string into string builder.
    identity.forEach(i -> {
      final StringBuilder key = new StringBuilder(i.getName()).append("_ids_" 
                                                                + i.getValue() + i.getType());
      final StringBuilder value = new StringBuilder(i.getValue()).append(':').append(i.getType());
      idMap.put(key.toString(),value.toString());
    });
    
    return idMap;
  } // End of creating KYC profile.
  
  /** Method to create portin profile. **/
  public Map<String,String> getPortinProfile(final PortinInfo portin) {
    
    final Map<String,String> portinMap = new ConcurrentHashMap<>();
    portinMap.put("portinDate_portinInfo",portin.getDate());
    portinMap.put("portinNumber_portinInfo",portin.getMsisdn());
    portinMap.put("portinDonor_portinInfo",portin.getDonor());
  
    return portinMap;
  } // End of creating portin profile.
  
  /** Method to create payment profile. **/
  private Map<String, String> getPaymentProfile(final PaymentInfo payment) {

    final Map<String,String> paymentMap = new ConcurrentHashMap<>();
    
    Optional.ofNullable(payment.getToken()).ifPresent(p -> 
                                            paymentMap.put("token_paymentInfo",payment.getToken()));
    Optional.ofNullable(payment.getMid()).ifPresent(p ->  
                                                paymentMap.put("mid_paymentInfo",payment.getMid()));
    Optional.ofNullable(payment.getLastDigits()).ifPresent(p -> 
                                  paymentMap.put("lastDigits_paymentInfo",payment.getLastDigits()));
    Optional.ofNullable(payment.getCardType()).ifPresent(p -> 
                                      paymentMap.put("cardType_paymentInfo",payment.getCardType()));
    Optional.ofNullable(payment.getBankName()).ifPresent(p ->  
                                      paymentMap.put("bankName_paymentInfo",payment.getBankName()));
    Optional.ofNullable(payment.getBank()).ifPresent(p -> 
                                              paymentMap.put("bank_paymentInfo",payment.getBank()));
    Optional.ofNullable(payment.getGateway()).ifPresent(p ->  
                                        paymentMap.put("gateway_paymentInfo",payment.getGateway()));
    
    return paymentMap;
  } // End of creating payment profile.
  
  /**
   * The method will create account service information with the information from plan and MSISDN
   * output FList.
   * 
   * @param: name-       @Mandatory - Base plan name.
   * @param: planReadOp- @Mandatory - Base plan output FList.
   * @param: devicePoid- @Mandatory - MSISDN.
  */
  public List<Services> createServices(
             final String name,final FList planReadOp,final Poid devicePoid) throws EBufException {
    
    final List<FList> services = planReadOp.get(FldServices.getInst()).getValues();
    final List<Services> serviceList = new ArrayList<>();
    int i = 0;
    for (final FList service : services) {
      final List<Deal> deals = createServiceDeals(service);
      final Devices device = 
            Devices.builder().elem("0").deviceObj(devicePoid.toString()).flags("1").build();
      
      serviceList.add(Services.builder().elem(String.valueOf(i)).passwordClear("XXXX").login(name)
                 .subscriptionObj("").servicedId(service.get(FldServiceId.getInst()))
                 .subscriptionIndex(service.get(FldSubscriptionIndex.getInst()).toString())
                 .balInfoIndex(service.get(FldBalInfoIndex.getInst()).toString())
                 .deals(deals).serviceObj(service.get(FldServiceObj.getInst()).toString())
                 .balInfo(BalanceInfo.builder().build()).devices(device).build());
      i++;
    } // End of looping through plan services.
    return serviceList;
  } // End of creating account service information from MSISDN and plan information.

  /**
   * The method will create limit arrays.
   * 
   * @param: planReadOp- @Mandatory - Base plan output FList.
  */
  public List<Limits> createLimitsArray(final FList planReadOp) throws EBufException {
    
    final List<FList> planLimits = planReadOp.get(FldLimit.getInst()).getValues();
    final List<Limits> limitsList = new ArrayList<>();
    int i = 0;
    List<String> indexList = new ArrayList<>();
    Enumeration<Integer> enum1 = planReadOp.get(FldLimit.getInst()).getKeyEnumerator();
    while (enum1.hasMoreElements()) {
      int arrIndex = enum1.nextElement().intValue();
      indexList.add(String.valueOf(arrIndex));
    }

    for (final FList limits : planLimits) {
      limitsList.add(Limits.builder().elem(indexList.get(i))
                    .creditLimit(String.valueOf(limits.get(FldCreditLimit.getInst())))
                    .creditFloor(String.valueOf(limits.get(FldCreditFloor.getInst())))
                    .creditThresholds(String.valueOf(limits.get(FldCreditThresholds.getInst())))
              .creditThresholdsFixed(String.valueOf(limits.get(FldCreditThresholdsFixed.getInst())))
              .build());
      i++;
    } // End of looping through plan services.
    return limitsList;
  } // End of creating account service information from MSISDN and plan information.
  
  /* The method create list of deals associated for a given service like SMS / GPRS.. **/
  private List<Deal> createServiceDeals(final FList service) throws EBufException {
    
    final List<Deal> dealsList = new ArrayList<>();
    
    for (int j = 0;j < service.get(FldDeals.getInst()).size();j++) {
      final Deal deal = Deal.builder().elem(String.valueOf(j))
                                      .dealObj(service.get(FldDeals.getInst()).getValues()
                                      .get(j).get(FldDealObj.getInst()).toString()).build();
      dealsList.add(deal);
    } // End of looping through service deals.
    return dealsList;
  } // End of creating service deals list. 
  
  /** Method to get PVT time. */
  private DateTime getPvtTime() throws EBufException {
    FList planRead = new FList();
    planRead.set(FldPoid.getInst(),new Poid(1,-1,"/plan"));

    FList currentTime = serviceUtil.executeBrmFlist(planRead, PortalOp.GET_PIN_VIRTUAL_TIME);

    return UtcDateUtil.convertDateToDateTime(currentTime.get(FldVirtualT.getInst()));
  }
} // End of AccountProfiles, which create profile section of CUST_COMMIT opcode.